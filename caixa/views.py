# -*- encoding: utf-8 -*-
import json
import datetime
from django.shortcuts import render

def home(request):
    dia_de_hoje = datetime.datetime.now().isoformat()

    #Obtém dados de uma API no formato JSON
    dados = [
        {'nome':'Guilherme','assunto':'Comemoração de Aniversário','data':'2015-11-23T11:25:43.511Z'},
        {'nome':'Flávio','assunto':'Re: Comemoração de Aniversário','data':'2015-11-25T18:25:43.511Z'},
        {'nome':'Juliana','assunto':'Re: Comemoração de Aniversário','data':'2015-11-28T18:25:43.511Z'},
        {'nome':'Silvia','assunto':'Re: Comemoração de Aniversário','data':'2015-12-01T21:25:43.511Z'},
        {'nome':'Silvia','assunto':'Re: Comemoração de Aniversário','data':'2015-12-10T01:25:43.511Z'},
        {'nome':'Ana Carolina','assunto':'Contas do fim do mês','data':'2015-12-08T18:25:43.511Z'},
        {'nome':'Guilherme','assunto':'Re: Contas do fim do mês','data':'2015-12-09T18:25:43.511Z'},
        {'nome':'Guilherme','assunto':'Reveillon','data':'2015-12-09T18:25:43.511Z'},
        {'nome':'Flávia','assunto':'Receita de bolo de cenoura','data':'2015-12-09T18:25:43.511Z'},
        {'nome':'Silvia','assunto':'Férias 2016','data':'2015-12-09T18:25:43.511Z'},
        {'nome':'Guilherme','assunto':'Re: Férias 2016','data':'2015-12-11T18:25:43.511Z'},
        {'nome':'Pedro','assunto':'Cartinha para o Papai Noel','data':'2015-12-11T18:25:43.511Z'},
        {'nome':'SIEVE','assunto':'Parabéns você foi contratado','data':'%s' %(dia_de_hoje)},
        {'nome':'SIEVE','assunto':'Documentos para Contratação :)','data':'%s' %(dia_de_hoje)},
        {'nome':'Duda','assunto':'Aniversário','data':'2015-12-11T18:25:43.511Z'},
    ]

    json_dados = json.dumps(dados, ensure_ascii=True, encoding='utf8')

    return render (request, 'index.html', locals())
